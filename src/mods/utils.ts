import { InjectionKey, inject } from "vue"
import { Axis } from "./types"

export const isSafeNumber: (v: unknown) => v is number = Number.isFinite as (v: unknown) => v is number

export function strictInject<T>(k: InjectionKey<T>) {
	const t = inject(k)
	if (t) return t
	throw new Error("Bad Injection. Probably missing " + k.toString())
}

export function strictAxisInject<T extends Axis>(k: InjectionKey<T>) {
	const t = inject(k)
	if (t?.name) return t
	throw new Error("Bad Axis Injection. Probably missing " + k.toString())
}

export function barryCenter(
	[fromX, fromY]: Readonly<[number, number]>,
	[toX, toY]: Readonly<[number, number]>,
	fraction: number,
): [number, number] {
	return [toX + (fromX - toX) * fraction, toY + (fromY - toY) * fraction]
}

function getOrder4Int(n: number) {
	// only used whith finite number
	// let r = 1
	// while (!Number.isInteger(n)) {
	// 	n = fixNumber(n * 10)
	// 	r *= 10
	// }
	// return r
	return Math.pow(10, n.toString().split(".")[1]?.length ?? 0)
}

export function orderOf(n: number) {
	const nb = Math.abs(n)
	if (isSafeNumber(nb)) {
		if (nb >= 1) return Math.pow(10, Math.trunc(Math.log10(nb)))
		else if (nb === 0) return 0
		else return Math.pow(10, Math.trunc(Math.log10(nb)) - 1)
	} else {
		throw new Error("orderOf: need safe number")
	}
}

/**
 * PGCD in french
 * not real gcd because the function try to handle float value
 * @param a
 * @param b
 * @returns greatest common divisor
 */
export function gcd(a: number, b: number): number {
	a = Math.abs(a)
	b = Math.abs(b)
	if (Number.isInteger(a) && Number.isInteger(b)) {
		do {
			const r = a
			a = b
			b = r % a
		} while (b > 0)
		return a
	} else if (Number.isFinite(a) && Number.isFinite(b)) {
		const i = getOrder4Int(a)
		const j = getOrder4Int(b)
		const dec = i > j ? i : j
		const tmp = gcd(a * dec, b * dec)
		return tmp / dec
	} else if (Number.isFinite(a)) return a
	else if (Number.isFinite(b)) return b
	throw new Error("gcd: cannot find gcd between " + a + " & " + b)
}

/**
 * PPCM in french
 * @param a
 * @param b
 * @returns lowest common multiple
 */
export function lcm(a: Readonly<number>, b: Readonly<number>): number {
	if (Number.isInteger(a) && Number.isInteger(b)) {
		return Math.abs((a * b) / gcd(a, b))
	}
	if (Number.isFinite(a) && Number.isFinite(b)) {
		const i = getOrder4Int(a)
		const j = getOrder4Int(b)
		const dec = i > j ? i : j
		const tmp = lcm(a * dec, b * dec)
		return tmp / dec
	}
	throw new Error("lcm: Need 2 safe number")
}

export function cheatLcm(a: Readonly<number>, b: Readonly<number>) {
	if (Math.max(a, b) > 2) {
		if (b < a) [a, b] = [b, a]
		// do nothing if args already integer
		a = Math.floor(a)
		b = Math.ceil(b)
	}
	return lcm(a, b)
}

/**
 * xor exist in js (^) but works with number
 * @param a
 * @param b
 * @returns xor result
 */
export function xor(a: Readonly<boolean>, b: Readonly<boolean>) {
	return a ? !b : b
}
