import { ComputedRef } from "vue"

export type Padding = [number, number, number, number]

export type Value = number | Date

export type ScaleF<T extends Value = Value> = (_: T) => number
export type UnscaleF<T extends Value = Value> = (value: number) => T

export type GraphAxis<T extends Value = Value> = {
	name: "x" | "y"
	size: ComputedRef<number>
	offset: ComputedRef<number>
	min: ComputedRef<T>
	max: ComputedRef<T>
	scale: ScaleF<T>
}

export type Axis<T extends Value = Value> = GraphAxis<T> & {
	date: T extends Date ? true : false
	reverse: ComputedRef<boolean>
	values: Map<symbol, T>
	multiples?: ComputedRef<number[]>
	setValue: (s: symbol, v: unknown) => void
	scaleSize: ScaleF<T>
	unscale: UnscaleF<T>
}
