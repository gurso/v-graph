export type Coord = { x: number; y: number }

/**
 *
 * @param option
 * @param option.angle - in radian
 * @param [option.radius=1]
 * @param [option.cx=0]
 * @param [option.cy=0]
 * @returns coordonate position
 */
export function polarToCartesian({
	cx = 0,
	cy = 0,
	radius = 1,
	angle,
}: {
	cx?: number
	cy?: number
	radius?: number
	angle: number
}) {
	return {
		x: cx + radius * Math.cos(angle),
		y: cy + radius * Math.sin(angle),
	}
}

export function diff(a: Coord, b: Coord) {
	return {
		x: b.x - a.x,
		y: b.y - a.y,
	}
}
export function distance(a: Coord, b: Coord) {
	const { x, y } = diff(a, b)
	return pytHypo(x, y)
}

export function pytHypo(adj: number, op: number) {
	return Math.sqrt(adj * adj + op * op)
}

export function pytRev(hypo: number, side: number) {
	return Math.sqrt(hypo * hypo - side * side)
}

export function radians2degrees(radians: number) {
	return radians * (180 / Math.PI)
}

export function degrees2radians(degrees: number) {
	return degrees * (Math.PI / 180)
}

export function getAngle(from: Coord, to: Coord) {
	const d = diff(to, from)
	const hypo = pytHypo(d.x, d.y)
	if (to.y >= from.y) {
		const r = Math.acos(d.x / hypo)
		return Math.PI - r
	} else if (to.x <= from.x) {
		const r = Math.asin(d.y / hypo)
		return r - Math.PI
	} else if (to.x > from.x) {
		const r = Math.acos(d.x / hypo)
		return r - Math.PI
	} else {
		throw new Error("Not possible")
	}
}

export function getAffineArgs(A: Coord, B: Coord) {
	const a = (B.y - A.y) / (B.x - A.x)
	const b = A.y - a * A.x
	return { a, b }
}

export function getAffineFunction(A: Coord, B: Coord) {
	const { a, b } = getAffineArgs(A, B)
	return function (x: number) {
		return a * x + b
	}
}

export function getCross(C: [Coord, Coord], D: [Coord, Coord]) {
	const { a, b } = getAffineArgs(...C)
	const { a: a2, b: b2 } = getAffineArgs(...D)
	if (a === a2) throw new Error("line don't cross //")
	const x = (b2 - b) / (a - a2)
	const y = a * x + b // || a2 * x + b2

	return { x, y }
}
