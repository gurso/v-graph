import { watchEffect, computed, ExtractPropTypes } from "vue"
import { ky, kx } from "../mods/axis"
import { d, isSafeValue, safeNumber, safeValue } from "../mods/props"
import { strictAxisInject, isSafeNumber } from "../mods/utils"

export const propsOptions = {
	x: safeValue,
	y: safeValue,
	x2: safeValue,
	y2: safeValue,
	...d,
	h: safeNumber,
	w: safeNumber,
	ignore: Boolean,
}

export function useRect(props: ExtractPropTypes<typeof propsOptions>) {
	const id = Symbol()
	const id2 = Symbol()
	const yAxis = strictAxisInject(ky)
	const xAxis = strictAxisInject(kx)
	watchEffect(() => {
		if (props.ignore) {
			xAxis.values.delete(id)
			xAxis.values.delete(id2)
			yAxis.values.delete(id)
			yAxis.values.delete(id2)
		} else {
			if (isSafeValue(props.x)) xAxis.values.set(id, props.x)
			else if (isSafeNumber(props.x2) && props.w) xAxis.values.set(id, props.x2 + props.w)
			if (isSafeValue(props.x2)) xAxis.values.set(id2, props.x2)
			else if (isSafeNumber(props.x) && props.w) xAxis.values.set(id2, props.x + props.w)

			if (isSafeValue(props.y)) yAxis.values.set(id, props.y)
			else if (isSafeNumber(props.y2) && props.h) yAxis.values.set(id, props.y2 + props.h)
			if (isSafeValue(props.y2)) yAxis.values.set(id2, props.y2)
			else if (isSafeNumber(props.y) && props.h) yAxis.values.set(id2, props.y + props.h)
		}
	})
	const fx = computed((): number => {
		if (isSafeValue(props.x)) {
			if (xAxis.reverse.value) {
				return xAxis.scale(props.x) - (fw.value || 0) + props.dx
			} else {
				return xAxis.scale(props.x) + props.dx
			}
		} else if (isSafeValue(props.x2) && isSafeNumber(fw.value)) {
			if (xAxis.reverse.value) {
				return xAxis.scale(props.x2) + props.dx
			} else {
				return xAxis.scale(props.x2) - fw.value + props.dx
			}
		} else throw new Error("Missing x or x2 props in Rect component")
	})
	const fy = computed((): number => {
		if (isSafeValue(props.y)) {
			if (yAxis.reverse.value) {
				return yAxis.scale(props.y) - (fh.value || 0) + props.dy
			} else {
				return yAxis.scale(props.y) + props.dy
			}
		} else if (isSafeValue(props.y2) && isSafeNumber(fh.value)) {
			if (yAxis.reverse.value) {
				return yAxis.scale(props.y2) + props.dy
			} else {
				return yAxis.scale(props.y2) - fh.value + props.dy
			}
		} else throw new Error("Missing y or y2 props in Rect component")
	})
	const fw = computed((): number | undefined => {
		if (isSafeNumber(props.w)) return xAxis.scaleSize(props.w)
		else if (isSafeValue(props.x) && isSafeValue(props.x2))
			return Math.abs(xAxis.scale(props.x2) - xAxis.scale(props.x))
		else return undefined
	})
	const fh = computed((): number | undefined => {
		if (isSafeNumber(props.h)) return yAxis.scaleSize(props.h)
		else if (isSafeValue(props.y) && isSafeValue(props.y2))
			return Math.abs(yAxis.scale(props.y2) - yAxis.scale(props.y))
		else return undefined
	})
	return { fx, fy, fw, fh }
}
