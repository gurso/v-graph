import { createApp } from "vue"
import Vg from "./lib"
import App from "./App.vue"

createApp(App).use(Vg).mount("#app")
