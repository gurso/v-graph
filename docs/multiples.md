<script setup>
import VgGraph from "../src/components/VgGraph.vue"
import VgScaledGrid from "../src/components/VgScaledGrid.vue"
import VgGraphScaleY from "../src/components/VgGraphScaleY.vue"
import VgGraphScaleX from "../src/components/VgGraphScaleX.vue"
import VgScaledCircle from "../src/components/VgScaledCircle.vue"
import VgScaledTicks from "../src/components/VgScaledTicks.vue"

const order = Math.random() * 100
const data = Array.from({length: 10}, () => order + Math.random())
</script>

# Les multiples

Il arrive souvent qu'on souhaite visualiser des valeurs avec un ordre de grandeur
variable. Dans les cas précédent, les valeurs minimums et maximums étaient décidées
à l'avance étant donné qui l'on souhaitait afficher des pourcentages.

Les cas d'usages peuvent être plus compliqués.
Si par exemple l'on souhaite surveiller un jeu de
données qui peut varier fortement ou faiblement, on peut laisser l'échelle
se définir automatiquement mais on risque d'avoir des valeurs
minimum et maximum peu harmonieuses.

Prenons un jeu de données générées aléatoirement et affichons les en nuage de points :

```js
const order = Math.random() * 100
const data = Array.from({ length: 10 }, () => order + Math.random())
```

Avec cet algo, les données peuvent varier entre 0 et 100, mais l'écart
entre les valeurs ne va pas excéder 1.

```vue-html
<VgGraph :width="500" :height="200" :padding="[10, 30]">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledTicks :nb="3" axis="y" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="i" :cy="d" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" :padding="[10, 30]" class="graph ov">
	<VgGraphScaleY>
		<VgGraphScaleX>
			<VgScaledTicks :nb="3" axis="y" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="i" :cy="d" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

La librairy "zoom" automatiquement sur les valeurs, mais elles ne sont pas "harmonieuses".
On aimerait qu'elles soient arrondies.
On sait que ces valeurs sont comprises entre 0 et 100, mais si on les ajoute avec
`VgScaledValue`, les faibles écarts seront difficilement visibles à l'oeil.
Les composants `VgGraphScaleX` et `VgGraphScaleY` ont une propriété nommé `multiples` qui
prend en valeur un tableaux de nombres.  
Avec cette propriété, le minimum et le maximum de l'axe concerné seront des multiples
des valeurs du tableau fournit via `multiples`.

```vue-html{2}
<VgGraph :width="500" :height="200" :padding="[10, 30]">
	<VgGraphScaleY :multiples="[1]">
		<VgGraphScaleX>
			<VgScaledTicks :nb="3" axis="y" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="i" :cy="d" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" :padding="[10, 30]" class="graph">
	<VgGraphScaleY :multiples="[1]">
		<VgGraphScaleX>
			<VgScaledTicks :nb="3" axis="y" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="i" :cy="d" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>

## pretty

Il peut etre fastidieux de devoir calculer les multiples qui affichent des étiquettes
harmonieuses.

Les composant `VgScaled[XY]` ont la propriete `pretty` qui applique un algorithme
qui ajoute automatiquement un multiple d'une puissance de 10 à son axe.

Cette propriété peut potentiellement répondre à vos besoins.

```vue-html{2}
<VgGraph :width="500" :height="200" :padding="[10, 30]">
	<VgGraphScaleY pretty>
		<VgGraphScaleX>
			<VgScaledTicks :nb="3" axis="y" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="i" :cy="d" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
```

<VgGraph :width="500" :height="200" :padding="[10, 30]" class="graph">
	<VgGraphScaleY pretty>
		<VgGraphScaleX>
			<VgScaledTicks :nb="3" axis="y" />
			<VgScaledGrid stroke="black" stroke-width="1" />
			<VgScaledCircle v-for="(d, i) of data" :key="i" :cx="i" :cy="d" r="5" fill="purple" />
		</VgGraphScaleX>
	</VgGraphScaleY>
</VgGraph>
