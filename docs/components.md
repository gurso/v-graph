## Graph

| ancêtres | parents |                enfants                |
| :------: | :-----: | :-----------------------------------: |
|  aucun   |  aucun  | VgScaled[XY] ,Vg VgGraphScaleDate[XY] |

| Propriétés |        Type        | Description                      |
| ---------- | :----------------: | -------------------------------- |
| width      |       number       | la largeur du graphique          |
| height     |       number       | la hauteur du graphique          |
| padding    | number \| number[] | les marges internes du graphique |

## Les axes

### Scaled[Date][x/y]

| ancêtres | parents | enfants |
| :------: | :-----: | :-----: |
| VgGraph  | VgGraph | tous es |

| Propriétés  |   Type   | Description                                                |
| ----------- | :------: | ---------------------------------------------------------- |
| offset      |  number  | un décalage de l'axe                                       |
| size        |  number  | la longueur de l'axe                                       |
| reverse     | boolean  | est-ce que l'axe est inversé                               |
| multiples\* | number[] | des valeurs qui seront des multiple des extrémité de l'axe |

\* seulement pour les axes qui ne sont pas des axes de dates.

## Composants éléments

Certain des composant élément sont une surcouche aux balises SVG les plus courantes.
Sont ajoutées les propriétés qui vont calculer les attributs concernant la taille
et la position de l'élément.

### VgScaledCircle

| Propriétés |     Type     | Description                 |
| ---------- | :----------: | --------------------------- |
| cx         | number\|Date | la valeur sur l'axe des x   |
| cy         | number\|Date | la valeur sur l'axe des y   |
| dx         |    number    | un décalage sur l'axe des x |
| dy         |    number    | un décalage sur l'axe des y |

### VgScaledLine

| Propriétés |     Type     | Description                       |
| ---------- | :----------: | --------------------------------- |
| x1         | number\|Date | la valeur sur l'axe des x pour x1 |
| y1         | number\|Date | la valeur sur l'axe des y pour x2 |
| x2         | number\|Date | la valeur sur l'axe des x pour y1 |
| y2         | number\|Date | la valeur sur l'axe des y pour y2 |
| dx         |    number    | un décalage sur l'axe des x       |
| dy         |    number    | un décalage sur l'axe des y       |

### VgScaledText

| Propriétés |     Type     | Description               |
| ---------- | :----------: | ------------------------- |
| x          | number\|Date | la valeur sur l'axe des x |
| y          | number\|Date | la valeur sur l'axe des y |

Comme pour l'élément `text`, le texte s'écrit au milieu des balises.
Les propriétées `dx` et `dy` ne sont pas indiquées vu qu'elles sont fournies
nativement en svg pour les élément de type `text`.

```vue-html
<VgScaledText> hello </VgScaledText>
```

### VgScaledRect

| Propriétés |     Type     | Description                          |
| ---------- | :----------: | ------------------------------------ |
| x          | number\|Date | la valeur du cotés gauche            |
| y          | number\|Date | la valeur du haut du rectangle       |
| x2         | number\|Date | la valeur du cotés droit             |
| y2         | number\|Date | la valeur du bas du rectangle        |
| w          |    number    | la valeur de la largeur du rectangle |
| h          |    number    | la valeur de la hauteur du rectangle |
| dx         |    number    | un décalage sur l'axe des x          |
| dy         |    number    | un décalage sur l'axe des y          |

Les propriétées `x2` et `y2` permettent de positionné un rectangle par rapport
au cotés opposé habituellement.

Pour être fonctionnel, un rectangle à besoin d'aux moins 2 valeurs sur chaque axes.  
2 valeurs parmis x, x2, w et width pour l'axe des x.  
2 valeurs parmis y, y2, h et height pour l'axe des y.

### VgScaledCurve

Le composent crée un élément SVG `path`, tous les attributs non listé ci-dessous seront transmis du composent à l'élément.

| Propriétés |              Type              | Description                                                      |
| ---------- | :----------------------------: | ---------------------------------------------------------------- |
| data       | [number\|Date, number\|Date][] | Une liste de valeur x, y                                         |
| type       |            function            |                                                                  |
| z          |            boolean             | Pour fermer la coube (que le 1er et dernier point se rejoignent) |

## Composant utilitaires

### VgScaledValue

### VgScaledTick

### VgScaledTicks

### VgScaledGrid
