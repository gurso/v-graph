import { describe, assert, it } from "vitest"
import { orderOf, gcd, lcm } from "../src/mods/utils"

describe("orderOf", () => {
	it("positive integers", () => {
		assert.equal(orderOf(0), 0)
		assert.equal(orderOf(1), 1)
		assert.equal(orderOf(9), 1)
		assert.equal(orderOf(15), 10)
		assert.equal(orderOf(97), 10)
		assert.equal(orderOf(122), 100)
	})

	it("negative integers", () => {
		assert.equal(orderOf(-1), 1)
		assert.equal(orderOf(-9), 1)
		assert.equal(orderOf(-10), 10)
	})

	it("decimal numbers < 1", () => {
		assert.equal(orderOf(4.6), 1)
		assert.equal(orderOf(0.73), 0.1)
		assert.equal(orderOf(-0.023), 0.01)
	})
})

describe("gcd", () => {
	it("positive integers", () => {
		assert.equal(gcd(0, 42), 42)
		assert.equal(gcd(1, 42), 1)
		assert.equal(gcd(8, 20), 4)
		assert.equal(gcd(10, 15), 5)
		assert.equal(gcd(0, 99), 99)
		assert.equal(gcd(Infinity, 42), 42)
		assert.equal(gcd(NaN, 42), 42) // to debate !
	})

	it("negative integers", () => {
		assert.equal(gcd(8, -20), 4)
		assert.equal(gcd(-10, -15), 5)
	})

	it("float values", () => {
		assert.equal(gcd(8.4, 4.22), 0.02)
		assert.equal(gcd(10, 1.25), 1.25)
	})
	it("float and integer", () => {
		assert.equal(gcd(1.75, 2), 0.25)
	})
})

describe("lcm", () => {
	it("positive integers", () => {
		assert.equal(lcm(10, 20), 20)
		assert.equal(lcm(4, 10), 20)
	})

	it("float value", () => {
		assert.equal(lcm(2.2, 8.8), 8.8)
	})
})
