import { describe, assert, it } from "vitest"
import { scale, unscale } from "../src/mods/scale"

describe("scale", () => {
	it("simple case", () => {
		const value = 50,
			max = 100,
			min = 0,
			size = 1000
		assert.equal(scale({ value, max, min, size, offset: 0 }), 500)
	})
})

describe("unscale", () => {
	it("simple case", () => {
		const value = 500,
			max = 100,
			min = 0,
			size = 1000
		assert.equal(unscale({ value, max, min, size, offset: 0 }), 50)
	})
})
